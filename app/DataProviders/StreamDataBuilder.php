<?php

namespace App\DataProviders;

use App\Models\StreamAnalytics;
use Illuminate\Database\Eloquent\Builder;

class StreamDataBuilder
{
    /** @var Builder */
    protected $builder;

    public function __construct()
    {
        $this->builder = StreamAnalytics::query();
    }

    public function setCreatedAt($from, $to)
    {
        if ($from && $to) {
            $this->builder->whereBetween('created_at', [$from, $to]);
        } else {
            $this->builder->whereHas('streamImport', function (Builder $builder) {
                $builder->where('is_current', true);
            });
        }

        return $this;
    }

    public function setGamesId($games_id)
    {
        if ($games_id) {
            $this->builder->whereHas('streamImport', function (Builder $builder) use ($games_id) {
                return $builder->whereIn('game_id', $games_id);
            });
        }

        return $this;
    }

    public function getBuilder()
    {
        return $this->builder;
    }
}
