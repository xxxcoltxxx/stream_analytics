<?php

namespace App\Console;

use App\User;
use Illuminate\Console\Command;

class CreateUserCommand extends Command
{
    protected $signature = 'users:create {name : User name} {email : E-mail}';

    protected $description = 'Create user';

    public function handle()
    {
        $password = str_random(8);
        $user = new User();
        $user->name = $this->argument('name');
        $user->email = $this->argument('email');
        $user->password = bcrypt($password);
        $user->save();
        $this->info("User successfully created with email {$user->email} and password {$password}");
    }
}
