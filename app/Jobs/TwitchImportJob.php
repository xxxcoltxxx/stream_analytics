<?php

namespace App\Jobs;

use App\Models\Game;
use App\Models\StreamAnalytics;
use App\Models\StreamImport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Services\StreamAnalyticsClient\Twitch\TwitchClient;

class TwitchImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $token;

    /** @var Game */
    private $game;

    /**
     * @var string
     */
    private $nextPageHash;

    /**
     * @var StreamImport
     */
    private $import;

    /**
     * Create a new job instance.
     *
     * @param StreamImport $import
     * @param Game         $game
     * @param string       $token
     * @param string|null  $nextPageHash
     */
    public function __construct(StreamImport $import, Game $game, string $token = null, string $nextPageHash = null)
    {
        $this->import = $import;
        $this->game = $game;
        $this->nextPageHash = $nextPageHash;
        $this->token = $token;
    }

    public function handle(TwitchClient $twitchClient)
    {
        $twitchClient->setLogger(Log::channel('stream_analytics'));
        $twitchClient->setAccessToken($this->token);

        $streams = $twitchClient->getStreams(
            [$this->game->twitchGame->twitch_game_id],
            $this->nextPageHash
        );

        DB::transaction(function () use ($streams, $twitchClient) {
            StreamAnalytics::query()->insert($streams->toStreamAnalytics($this->import));

            if ($streams->hasNextPage()) {
                $job = new TwitchImportJob(
                    $this->import,
                    $this->game,
                    $twitchClient->getAccessToken(),
                    $streams->getNextPageHash()
                );
                dispatch($job->onQueue('twitch_imports'));
            } else {
                $this->import->markAsCurrent();
                $this->import->markAsCompleted();
            }
        });
    }
}
