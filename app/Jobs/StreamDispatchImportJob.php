<?php

namespace App\Jobs;

use App\Models\Game;
use App\Models\StreamImport;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class StreamDispatchImportJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    public function handle()
    {
        Game::query()->each(function (Game $game) {
            /** @var StreamImport $import */
            $import = $game->streamImports()->create([
                'provider'   => 'twitch',
                'created_at' => now(),
            ]);

            $job = new TwitchImportJob($import, $game);
            dispatch($job->onQueue('twitch_imports'));
        });
    }
}
