<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int         id
 * @property int         game_id
 * @property int         twitch_game_id
 * @property Carbon|null created_at
 * @property Carbon|null updated_at
 */
class TwitchGame extends Model
{
    protected $fillable = [
        'game_id',
        'twitch_game_id',
    ];
}
