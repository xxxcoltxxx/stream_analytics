<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * @property int                            id
 * @property string                         title
 * @property Carbon|null                    created_at
 * @property Carbon|null                    updated_at
 * @property Carbon|null                    deleted_at
 * @property-read TwitchGame|null           twitchGame
 * @property-read Collection|StreamImport[] streamImports
 */
class Game extends Model
{
    use SoftDeletes;

    protected $fillable = [
        'title',
    ];

    public function twitchGame()
    {
        return $this->hasOne(TwitchGame::class);
    }

    public function streamImports()
    {
        return $this->hasMany(StreamImport::class);
    }
}
