<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;

/**
 * @property int               id
 * @property int               stream_import_id
 * @property int               stream_id
 * @property int               viewers_count
 * @property Carbon|null       created_at
 * @property-read StreamImport streamImport
 */
class StreamAnalytics extends Model
{
    protected $table = 'stream_analytics';

    public $timestamps = false;

    protected $fillable = [
        'stream_import_id',
        'stream_id',
        'viewers_count',
        'created_at',
    ];

    protected $dates = [
        'created_at',
    ];

    public function streamImport()
    {
        return $this->belongsTo(StreamImport::class);
    }
}
