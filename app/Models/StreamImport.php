<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Carbon;
use Illuminate\Support\Collection;

/**
 * @property int                               id
 * @property int                               game_id
 * @property string                            provider
 * @property bool                              is_completed
 * @property bool                              is_current
 * @property Carbon                            created_at
 * @property-read Collection|StreamAnalytics[] streamAnalytics
 * @property-read Game                         game
 */
class StreamImport extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'game_id',
        'provider',
        'is_current',
        'is_completed',
        'created_at',
    ];

    public function streamAnalytics()
    {
        return $this->hasMany(StreamAnalytics::class);
    }

    public function game()
    {
        return $this->belongsTo(Game::class);
    }

    public function markAsCurrent()
    {
        static::query()
            ->where('is_current', true)
            ->where('game_id', $this->game_id)
            ->update(['is_current' => false]);

        $this->update(['is_current' => true]);

        return $this;
    }

    public function markAsCompleted()
    {
        $this->update(['is_completed' => true]);

        return $this;
    }
}
