<?php

namespace App\Http\Controllers;

use App\DataProviders\StreamDataBuilder;

class StreamsController extends Controller
{
    public function index(StreamIndexRequest $request, StreamDataBuilder $dataBuilder)
    {
        $builder = $dataBuilder
            ->setCreatedAt($request->input('created_at.from'), $request->input('created_at.to'))
            ->setGamesId($request->input('games_id'))
            ->getBuilder();

        $total = $builder->count();

        $builder
            ->orderBy('id')
            ->limit($request->input('pagination.limit', 10))
            ->offset($request->input('pagination.offset'));

        return [
            'data'       => $builder->pluck('stream_id'),
            'pagination' => [
                'total' => $total,
            ],
        ];
    }

    public function viewers(StreamIndexRequest $request, StreamDataBuilder $dataBuilder)
    {
        $builder = $dataBuilder
            ->setCreatedAt($request->input('created_at.from'), $request->input('created_at.to'))
            ->setGamesId($request->input('games_id'))
            ->getBuilder();

        return [
            'data' => [
                'viewers_count' => $builder->sum('viewers_count'),
            ],
        ];
    }
}
