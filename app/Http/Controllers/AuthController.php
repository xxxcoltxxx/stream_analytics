<?php

namespace App\Http\Controllers;

use Illuminate\Auth\AuthManager;
use Illuminate\Http\Request;

class AuthController extends Controller
{
    /** @var AuthManager */
    private $authManager;

    public function __construct(AuthManager $authManager)
    {
        $this->authManager = $authManager;
    }

    public function signIn(Request $request)
    {
        $credentials = $request->only('email', 'password');

        $token = $this->authManager->guard()->attempt($credentials);
        if ($token) {
            return $this->respondWithToken($token);
        }

        return response()->json(['error' => 'Unauthorized'], 401);
    }

    protected function respondWithToken(string $token)
    {
        return response()->json([
            'data' => [
                'access_token' => $token,
                'token_type'   => 'bearer',
                'expires_in'   => $this->authManager->guard()->factory()->getTTL() * 60,
            ]
        ]);
    }
}
