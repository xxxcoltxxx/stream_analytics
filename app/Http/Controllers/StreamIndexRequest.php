<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Http\FormRequest;

class StreamIndexRequest extends FormRequest
{
    public function authorize()
    {
        return auth()->check();
    }

    public function rules()
    {
        return [
            'created_at'      => 'array',
            'created_at.from' => 'date',
            'created_at.to'   => 'date',
            'games_id'        => 'array',
            'games_id.*'      => 'integer',
        ];
    }
}
