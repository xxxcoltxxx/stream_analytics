<?php

return [
    'schedule' => ['minutes' => env('STREAM_SCHEDULE_MINUTES', 5)],
];
