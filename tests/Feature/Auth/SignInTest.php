<?php

namespace Tests\Feature\Auth;

use App\User;
use Illuminate\Http\Response;
use Lcobucci\JWT\Parser;
use Tests\TestCase;

class SignInTest extends TestCase
{
    /** @var User */
    protected $user;

    private $password = '12345678';

    protected function setUp()
    {
        parent::setUp();
        $this->user = factory(User::class)->create([
            'password' => bcrypt($this->password),
        ]);
    }

    public function test_return_jwt()
    {
        $response = $this->postJson(route('auth.sign-in'), [
            'email'    => $this->user->email,
            'password' => $this->password,
        ]);

        $response->assertSuccessful();
        $jwt = new Parser();
        $payload = $jwt->parse($response->json('data.access_token'));
        $this->assertEquals($this->user->id, $payload->getClaim('sub'));
    }

    public function test_unauthorized()
    {
        $response = $this->postJson(route('auth.sign-in'), [
            'email'    => $this->user->email,
            'password' => 'wrong password',
        ]);

        $response->assertStatus(Response::HTTP_UNAUTHORIZED);
    }
}
