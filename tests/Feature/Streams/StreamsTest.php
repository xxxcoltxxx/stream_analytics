<?php

namespace Tests\Feature\Streams;

use App\Models\StreamAnalytics;
use App\Models\StreamImport;
use App\User;
use Illuminate\Support\Carbon;
use Tests\TestCase;
use Tymon\JWTAuth\Facades\JWTAuth;

class StreamsTest extends TestCase
{
    protected function setUp()
    {
        parent::setUp();

        $user = factory(User::class)->create();
        $this->defaultHeaders = [
            'Authorization' => 'Bearer ' . JWTAuth::fromUser($user),
        ];
    }

    public function test_no_filter()
    {
        $imports = collect([
            $this->createImport(now())->markAsCurrent(),
            $this->createImport(now())->markAsCurrent(),
        ]);

        $response = $this->getJson(route('streams.index'));

        $response->assertSuccessful();
        $this->assertEquals(
            $imports->flatMap->streamAnalytics->pluck('stream_id')->toArray(),
            $response->json('data')
        );
    }

    public function test_games_filter()
    {
        $this->createImport(now())->markAsCurrent();
        $import = $this->createImport(now())->markAsCurrent();

        $response = $this->getJson(route('streams.index', [
            'games_id' => [$import->game_id],
        ]));

        $response->assertSuccessful();
        $this->assertEquals(
            $import->streamAnalytics->pluck('stream_id')->toArray(),
            $response->json('data')
        );
    }

    public function test_date_filter()
    {
        $this->createImport(today()->subDays(2));
        $this->createImport(today()->addHour());
        $import = $this->createImport(today()->subHour());

        $response = $this->getJson(route('streams.index', [
            'created_at' => [
                'from' => today()->subDay()->toDateTimeString(),
                'to'   => today()->toDateTimeString(),
            ],
        ]));

        $response->assertSuccessful();
        $this->assertEquals(
            $import->streamAnalytics->pluck('stream_id')->toArray(),
            $response->json('data')
        );
    }

    public function validation_provider()
    {
        return [
            'games_id_not_array' => [['games_id' => 'str'], ['games_id']],
            'games_id_not_array_of_strings' => [['games_id' => ['str']], ['games_id.0']],
            'created_at_not_array' => [['created_at' => now()->toDateTimeString()], ['created_at']],
            'created_at_format' => [
                ['created_at' => ['from' => 'wrong', 'to' => 'wrong']],
                ['created_at.from', 'created_at.to']
            ],
        ];
    }

    /**
     * @param $params
     * @param $errors
     *
     * @dataProvider validation_provider
     */
    public function test_validation($params, $errors)
    {
        $response = $this->getJson(route('streams.index', $params));
        $response->assertJsonValidationErrors($errors);
    }

    private function createImport(Carbon $created_at): StreamImport
    {
        $import = factory(StreamImport::class)->create(compact('created_at'));
        factory(StreamAnalytics::class)->create([
            'stream_import_id' => $import->id,
            'created_at'       => $created_at->toDateTimeString(),
        ]);

        return $import;
    }
}
