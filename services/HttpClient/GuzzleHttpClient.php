<?php

namespace Services\HttpClient;

use GuzzleHttp\Client;
use Services\HttpClient\Serializers\Serializer;

class GuzzleHttpClient implements HttpClient
{
    /** @var Serializer */
    private $serializer;

    /** @var Client */
    private $client;

    /** @var string */
    private $baseUrl;

    /** @var array */
    private $defaultHeaders = [];

    public function __construct(Client $client, Serializer $serializer)
    {
        $this->client = $client;
        $this->serializer = $serializer;
    }

    public function setBaseUrl(?string $baseUrl = null): HttpClient
    {
        $this->baseUrl = $baseUrl;

        return $this;
    }

    public function setDefaultHeaders(array $headers = []): HttpClient
    {
        $this->defaultHeaders = $headers;

        return $this;
    }

    public function get(string $url, array $queryParams = [], array $headers = [])
    {
        $body = $this->client->get($this->fullUrl($url, $queryParams), [
            'headers' => $this->headers($headers),
        ])->getBody()->getContents();

        return $this->serializer->parse($body);
    }

    public function post(string $url, array $params = [], array $headers = [])
    {
        $body = $this->client->get($this->fullUrl($url), [
            'headers'     => $this->headers($headers),
            'form_params' => $params,
        ])->getBody()->getContents();

        return $this->serializer->parse($body);
    }

    public function put(string $url, array $params = [], array $headers = [])
    {
        $body = $this->client->put($this->fullUrl($url), [
            'headers'     => $this->headers($headers),
            'form_params' => $params,
        ])->getBody()->getContents();

        return $this->serializer->parse($body);
    }

    public function delete(string $url, array $params = [], array $headers = [])
    {
        $body = $this->client->delete($this->fullUrl($url), [
            'headers'     => $this->headers($headers),
            'form_params' => $params,
        ])->getBody()->getContents();

        return $this->serializer->parse($body);
    }

    private function fullUrl(string $url, $queryParams = []): string
    {
        $fullUrl = rtrim($this->baseUrl ?? '', '/') . '/' . ltrim($url, '/');

        if (! empty($queryParams)) {
            return $fullUrl . '?' . http_build_query($queryParams);
        }

        return $fullUrl;
    }

    private function headers(array $headers): array
    {
        return array_merge($this->defaultHeaders, $headers);
    }
}
