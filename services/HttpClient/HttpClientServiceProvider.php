<?php

namespace Services\HttpClient;

use Illuminate\Support\ServiceProvider;
use Services\HttpClient\Serializers\JsonSerializer;
use Services\HttpClient\Serializers\Serializer;

class HttpClientServiceProvider extends ServiceProvider
{
    protected $defer = true;

    public function register(): void
    {
        $this->app->bind(Serializer::class, JsonSerializer::class);
        $this->app->bind(HttpClient::class, GuzzleHttpClient::class);
    }

    public function provides(): array
    {
        return [
            Serializer::class,
            HttpClient::class,
        ];
    }
}
