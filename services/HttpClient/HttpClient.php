<?php

namespace Services\HttpClient;

interface HttpClient
{
    public function setBaseUrl(?string $baseUrl = null): HttpClient;

    public function setDefaultHeaders(array $headers = []): HttpClient;

    public function get(string $url, array $queryParams = [], array $headers = []);

    public function post(string $url, array $params = [], array $headers = []);

    public function put(string $url, array $params = [], array $headers = []);

    public function delete(string $url, array $params = [], array $headers = []);
}
