<?php

namespace Services\HttpClient\Exceptions;

class HttpResponseParseException extends \Error
{
    protected $content = '';

    public function __construct(string $message = '', $content = null)
    {
        parent::__construct($message, 400);
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }

}
