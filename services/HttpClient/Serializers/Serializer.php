<?php

namespace Services\HttpClient\Serializers;

interface Serializer
{
    public function parse($body);
}
