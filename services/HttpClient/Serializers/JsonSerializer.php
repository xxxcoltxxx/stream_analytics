<?php

namespace Services\HttpClient\Serializers;

use Services\HttpClient\Exceptions\HttpResponseParseException;

class JsonSerializer implements Serializer
{
    public function parse($body)
    {
        if ($body === null) {
            return $body;
        }

        $json = json_decode($body, true);

        throw_if($json === false, new HttpResponseParseException('Response does not contain json', $body));
        throw_if(
            json_last_error() !== 0,
            new HttpResponseParseException(sprintf('Cannot parse to json: %s', json_last_error_msg()), $body)
        );
        throw_unless(is_array($json), new HttpResponseParseException('Json parse result is not array', $body));

        return $json;
    }
}
