<?php

namespace Services\StreamAnalyticsClient\Twitch;

use App\Models\StreamImport;

interface StreamPage
{
    public function toStreamAnalytics(StreamImport $import): array;

    public function hasNextPage(): bool;

    public function getNextPageHash(): string;
}
