<?php

namespace Services\StreamAnalyticsClient\Twitch;

use App\Models\StreamImport;

class TwitchPage implements StreamPage
{
    /**
     * @var array
     */
    private $streams;

    private $nextPage;

    private $created_at;

    public function __construct(array $streams, $nextPage)
    {
        $this->streams = $streams;
        $this->nextPage = $nextPage;
        $this->created_at = now()->toDateTimeString();
    }

    public function toStreamAnalytics(StreamImport $import): array
    {
        return array_map(function (array $item) use ($import) {
            return [
                'stream_import_id' => $import->id,
                'stream_id'        => $item['id'],
                'viewers_count'    => $item['viewer_count'] ?? 0,
                'created_at'       => $this->created_at,
            ];
        }, $this->streams);
    }

    public function hasNextPage(): bool
    {
        return (bool) $this->nextPage;
    }

    public function getNextPageHash(): string
    {
        return $this->nextPage;
    }
}
