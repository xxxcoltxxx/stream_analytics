<?php

namespace Services\StreamAnalyticsClient\Twitch;

use Psr\Log\LoggerInterface;
use Services\HttpClient\HttpClient;
use Services\StreamAnalyticsClient\StreamClient;

class TwitchClient implements StreamClient
{
    /** @var string */
    private $uniqId;

    private $accessToken;

    /** @var HttpClient */
    private $httpClient;

    /** @var LoggerInterface */
    private $logger;

    public function __construct(HttpClient $httpClient)
    {
        $this->httpClient = $httpClient;
        $this->httpClient->setBaseUrl('https://api.twitch.tv/helix');
        $this->uniqId = uniqid('twitch', true);
    }

    public function setLogger(LoggerInterface $logger): void
    {
        $this->logger = $logger;
    }

    private function authorize(): void
    {
        $provider = new TwitchProvider([
            'clientId'     => config('services.twitch.client_id'),
            'clientSecret' => config('services.twitch.client_secret'),
            'redirectUri'  => config('services.twitch.redirect_uri'),
        ]);

        $this->accessToken = $provider->getAccessToken('client_credentials')->getToken();
        $this->setAccessToken($this->accessToken);
    }

    public function getAccessToken()
    {
        return $this->accessToken;
    }

    public function setAccessToken(?string $token)
    {
        $this->accessToken = $token;
        if ($token) {
            $this->httpClient->setDefaultHeaders([
                'Authorization' => "Bearer {$token}",
            ]);
        }
    }

    public function getStreams(array $gameIds, string $after = null): StreamPage
    {
        if (! $this->accessToken) {
            $this->authorize();
        }
        $params = ['first' => 100, 'game_id' => $gameIds];

        if ($after) {
            $params['after'] = $after;
        }

        $this->logInfo('Load page', ['games' => $gameIds, 'uniqueId' => $this->uniqId]);
        $response = $this->httpClient->get('streams', $params);

        return new TwitchPage($response['data'] ?? [], $response['pagination']['cursor'] ?? null);
    }

    private function logInfo(string $message, array $context = []): void
    {
        $this->log('info', $message, $context);
    }

    private function log($level, string $message, array $context = []): void
    {
        if (! $this->logger) {
            return;
        }

        $this->logger->log($level, $message, $context);
    }
}
