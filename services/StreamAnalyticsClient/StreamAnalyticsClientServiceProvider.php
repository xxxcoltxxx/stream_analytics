<?php

namespace Services\StreamAnalyticsClient;

use Illuminate\Support\ServiceProvider;

class StreamAnalyticsClientServiceProvider extends ServiceProvider
{
    protected $defer = true;
}
