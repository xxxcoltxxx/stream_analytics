<?php

namespace Services\StreamAnalyticsClient;

use Services\StreamAnalyticsClient\Twitch\StreamPage;

interface StreamClient
{
    public function getStreams(array $gameIds): StreamPage;
}
