<?php

use App\Models\Game;
use App\Models\TwitchGame;
use Illuminate\Support\Facades\Artisan;

Artisan::command('twitch:seed', function () {
    /** @var Game $game */
    $game = Game::query()->firstOrCreate(['title' => 'Fortnite']);
    TwitchGame::query()->updateOrCreate(
        ['game_id' => $game->id],
        ['twitch_game_id' => 33214]
    );
})->describe('Seed a games');
