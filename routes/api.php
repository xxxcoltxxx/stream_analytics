<?php

use App\Http\Controllers\AuthController;
use App\Http\Controllers\StreamsController;
use Illuminate\Support\Facades\Route;
use Services\HttpClient\HttpClient;

Route::middleware('guest')->group(function () {
    Route::post('auth/sign-in', [AuthController::class, 'signIn'])->name('auth.sign-in');
});

Route::middleware('auth:api')->group(function () {
    Route::get('streams', [StreamsController::class, 'index'])->name('streams.index');
    Route::get('streams/viewers-count', [StreamsController::class, 'viewers'])->name('streams.viewers');
});
