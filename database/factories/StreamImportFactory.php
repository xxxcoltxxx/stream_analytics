<?php

use App\Models\Game;
use App\Models\StreamImport;
use Faker\Generator as Faker;

$factory->define(StreamImport::class, function (Faker $faker) {
    $providers = ['twitch'];

    return [
        'game_id'      => factory(Game::class),
        'provider'     => $faker->randomElement($providers),
        'is_completed' => true,
        'is_current'   => false,
        'created_at'   => now()->toDateTimeString(),
    ];
});
