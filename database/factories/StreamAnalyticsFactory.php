<?php

use App\Models\StreamAnalytics;
use App\Models\StreamImport;
use Faker\Generator as Faker;

$factory->define(StreamAnalytics::class, function (Faker $faker) {
    return [
        'stream_import_id' => factory(StreamImport::class),
        'stream_id'        => $faker->randomDigitNotNull,
        'viewers_count'    => $faker->randomDigitNotNull,
        'created_at'       => now()->toDateTimeString(),
    ];
});
