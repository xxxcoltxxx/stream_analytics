<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UpdateViewersCountType extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('stream_analytics', function (Blueprint $table) {
            $table->unsignedBigInteger('viewers_count')->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('stream_analytics', function (Blueprint $table) {
            $table->unsignedInteger('viewers_count')->change();
        });
    }
}
