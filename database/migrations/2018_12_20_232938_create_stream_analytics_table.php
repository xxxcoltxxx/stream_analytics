<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreamAnalyticsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stream_analytics', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('stream_import_id')->index();
            $table->unsignedBigInteger('stream_id');
            $table->unsignedInteger('viewers_count');
            $table->timestamps();

            $table->foreign('stream_import_id')->references('id')->on('stream_imports');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stream_analytics');
    }
}
