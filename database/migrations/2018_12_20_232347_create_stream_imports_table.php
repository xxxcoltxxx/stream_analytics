<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStreamImportsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stream_imports', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedBigInteger('game_id')->index();
            $table->enum('provider', ['twitch']);
            $table->boolean('is_completed')->default(false);
            $table->timestamp('created_at')->index();

            $table->foreign('game_id')->references('id')->on('games');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stream_imports');
    }
}
