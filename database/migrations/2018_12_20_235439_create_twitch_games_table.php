<?php

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTwitchGamesTable extends Migration
{
    public $withinTransaction = true;

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('twitch_games', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('game_id')->index();
            $table->unsignedBigInteger('twitch_game_id');
            $table->timestamps();

            $table->foreign('game_id')->references('id')->on('games');
        });

        DB::statement('
          INSERT INTO twitch_games (game_id, twitch_game_id)
          (SELECT id, game_id FROM games)
        ');

        Schema::table('games', function (Blueprint $table) {
            $table->dropColumn('game_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('games', function (Blueprint $table) {
            $table->unsignedBigInteger('game_id')->index();
        });

        DB::statement('
          UPDATE games SET game_id = (SELECT twitch_game_id FROM twitch_games WHERE games.id = twitch_games.game_id)
        ');

        Schema::drop('twitch_games');
    }
}
